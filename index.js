const express=require('express');

const cors= require('cors');

const bodyParser=require('body-parser');

require("dotenv").config();

const db=require("./app/models");

const {initial}=require("./data");

const app=express();

app.use(cors());

app.use(express.json());

app.use(bodyParser.urlencoded({extended: false}));

const path=require('path');

db.mongoose.connect(`mongodb://${process.env.MONGODB_HOST}:${process.env.MONGODB_PORT}/${process.env.MONGODB_DATABASE}`)
.then(()=>{
    console.log("Connect mongoDb Successfully");
    initial();
})
.catch((error)=>{
    console.error("Connect error",error);
    process.exit();
})

app.get("/",(req,res)=>{
    console.log(__dirname);
    res.sendFile(path.join(__dirname+"/app/views/login.html"));
})

app.get("/signup",(req,res)=>{
    console.log(__dirname);
    res.sendFile(path.join(__dirname+"/app/views/signup.html"));
})

app.use(express.static(__dirname+"/app/views"));

app.use("/",require("./app/routers/authRouter"));

const PORT=process.env.ENV_PORT;
app.listen(PORT,()=>{
    console.log(`Sever is running on port ${PORT}`);
})