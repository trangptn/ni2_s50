const db = require("../models");
const bcrypt = require("bcrypt");
const jwt=require('jsonwebtoken');


const signUp = async (req, res) => {
    try {
        const userRole = await db.role.findOne({
            name: "user"
        })
        if(req.body.confirmpassword !== req.body.password )
        {
            res.status(401).json({
                message:"Mat khau khong trung khop"
            })
            return false;
        }
        const user = new db.user({
            username: req.body.username,
            password: bcrypt.hashSync(req.body.password, 8),
            roles: [
                userRole._id
            ]
        })
        await user.save();
        res.redirect('/');
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            message:"Internal server error"
        })
    }

}

const login=async (req,res)=>{
        await db.user.findOne({username:req.body.username}).populate("roles")
        .then((exitUser)=>{
       if(!exitUser)
       {
        return res.status(404).send({
            message:"User not found"
        })
       }

       var passwordIsValid=bcrypt.compareSync(
            req.body.password,
            exitUser.password
       )

       if(!passwordIsValid)
       {
        return res.status(401).json({
            message:"Invalid password"
        })
       }
      
       const secretKey=process.env.JWT_SECRET;
      const token= jwt.sign({id:exitUser._id},secretKey,{
        algorithm:"HS256",
        allowInsecureKeySizes:true,
        expiresIn:86400 //1 ngay
       })

       res.send("Dang nhap thanh cong");
})
    .catch((error)=>{
        console.log(error);
        res.status(500).json({
            message:"Intenal sever error"
        })
    })
}

module.exports = {
    signUp,
    login
}