const express=require('express');

const router=express.Router();

const authMiddleware=require("../middlewares/authMiddleware");
const authController=require("../controllers/authController");

router.post("/signup",authMiddleware.checkDuplicateUsername,authController.signUp);

router.post("/login",authController.login);

module.exports=router;